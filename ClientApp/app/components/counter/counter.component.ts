import { Component } from '@angular/core';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.css']
})
export class CounterComponent {
    public currentCount = 0;
    public values =  new Array<ScanData>();
    public newCode = "";
    public isValid = false;

    public incrementCounter() {
        this.currentCount++;
    }

    public scanNew(data: string) {
        this.values.push(
            new ScanData(data,1));
    }

    public scanNew1() {
        console.log(this.newCode);
        if (this.newCode != null )
        {
            this.values.push(new ScanData(this.newCode,1));
            this.newCode = "";
            this.isValid = this.values.length >= 10;
        }
    }
}

class ScanData {
    code: string;
    cnt: number;

    constructor(code:string, cnt:number){
        this.code = code;
        this.cnt = cnt;
    }
}
